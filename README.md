# DigiAgriApp presentations

This repository contains the presentations of DigiAgriApp projects.

You can see the presentation at https://digiagriapp.gitlab.io/digiagriapp-presentations/

## License
Slides license is <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank"><b>Creative Commons Share Alike 4.0</b></a>
