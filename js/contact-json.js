const contactjson = {
    "logoPosition": "right",
    "showCompletedPage": false,
    "pages": [
     {
      "name": "page1",
      "elements": [
       {
        "type": "text",
        "name": "name_surname",
        "title": "Name and surname",
        "isRequired": true
       },
       {
        "type": "text",
        "name": "email",
        "title": "Email address",
        "isRequired": true,
        "inputType": "email"
       },
       {
        "type": "text",
        "name": "question",
        "title": "Message",
        "isRequired": true
       }
      ]
     }
    ]
   }